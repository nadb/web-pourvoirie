

document.addEventListener("DOMContentLoaded", function () {


    var connexion = new MovieDB();

    if(location.pathname.search("fiche-film.html") > 0){

        var params = (new URL(document.location)).searchParams;
        //console.log(params.get("id"));

        connexion.requeteInfoFilm(params.get("id"));

    }
    else{
        connexion.requeteFilmUpcoming();
        //connexion.requeteFilmCaroussel();

    }


    (function() {
        'use strict';


        function backToTop() {
            if (window.pageYOffset > 0) {
                window.scrollBy(0, -80);
                setTimeout(backToTop, 0);
            }
        }

        var goTopBtn = document.querySelector('.back_to_top');

        goTopBtn.addEventListener('click', backToTop);
    })();

    //----------Swiper----------//
    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 3,
        loop: true,
        spaceBetween: 30,
        autoplay: {
            delay: 5000,
        },
        navigation: {
            nextEl: '.suivant',
            prevEl: '.precedent',
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });



});


class MovieDB{

    constructor(){
        console.log("Parfait 2");

        this.APIKey = "eda01ad95b124c2be1b5f4308d87648f";

        this.lang = "fr-CA";

        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "https://image.tmdb.org/t/p/";

        this.largeurAffiche = ["92", "154", "185", "342", "500", "780"];

        this.largeurTeteAffiche = ["45", "185"];

        this.totalFilm = 8;

        this.totalActeur = 6;
    }

    requeteFilmUpcoming(){


        var xhr = new XMLHttpRequest();


        xhr.addEventListener("readystatechange", this.retourFilmUpcoming.bind(this));

        //xhr.open("GET", "https://api.themoviedb.org/3/movie/popular?page=1&language=en-US&api_key=%3C%3Capi_key%3E%3E");
        xhr.open("GET", this.baseURL + "movie/upcoming?page=1&language=" + this.lang + "&api_key=" + this.APIKey);

        xhr.send();
    }


    retourFilmUpcoming(e){

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).results;



            this.afficheFilmUpcoming(data)

        }

    }


    afficheFilmUpcoming(data){


        console.log( data );

        for(var i=0; i<this.totalFilm; i++){

            var unArticle = document.querySelector(".template > .film").cloneNode(true);


            unArticle.querySelector("h1").innerText = data[i].title;

            if(data[i].overview === ""){
                unArticle.querySelector(".description").innerText = "Dû a des propos raciste/homophobes et irrespectueux, la description de cet article as été retirée ";
            }
            else{
                unArticle.querySelector(".description").innerText = data[i].overview;
            }

            unArticle.querySelector("img").setAttribute("src", this.imgPath + "w500" + data[i].poster_path);
            unArticle.querySelector("img").setAttribute("alt", data[i].title);

            unArticle.querySelector("a").setAttribute("href", "fiche-film.html?id=" + data[i].id);

            document.querySelector(".liste-films").appendChild(unArticle);
        }

    }


    //----------afficher un film---------------

    requeteInfoFilm(movieId){

        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourInfoFilm.bind(this));

        //xhr.open("GET", "https://api.themoviedb.org/3/movie/%7Bmovie_id%7D?language=en-US&api_key=%3C%3Capi_key%3E%3E");
        xhr.open("GET", this.baseURL + "movie/"+ movieId +"?language="+this.lang+"&api_key="+this.APIKey);

        xhr.send();
    }

    retourInfoFilm(e){

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText);



            this.afficheInfoFilm(data);

        }

    }

    afficheInfoFilm(data){

        console.log(data);


        document.querySelector(".fiche-film>h1").innerText = data.title;
        document.querySelector(".fiche-film>img").setAttribute("src", this.imgPath + "w500" + data.poster_path);
        document.querySelector(".fiche-film>img").setAttribute("alt", data.title);

        if(data.overview === ""){
            document.querySelector(".description").innerText = "Dû a des propos raciste/homophobes et irrespectueux, la description de cet article as été retirée ";
        }
        else{
            document.querySelector(".description").innerText = data.overview;
        }


        this.requeteActeur(data.id);

    }

    //----------afficher un acteur---------------

    requeteActeur(movieId){

        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourActeur.bind(this));

        //xhr.open("GET", "https://api.themoviedb.org/3/movie/%7Bmovie_id%7D/credits?api_key=%3C%3Capi_key%3E%3E");
        xhr.open("GET", this.baseURL + "movie/" + movieId + "/credits?api_key=" + this.APIKey);

        xhr.send();
    }

    retourActeur(e){

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).cast;



            this.afficheActeur(data);

        }

    }

    afficheActeur(data){

        console.log(data);


        for(var i=0; i<this.totalActeur; i++){

            var unActeur = document.querySelector(".template > .acteur").cloneNode(true);


            unActeur.querySelector("h1").innerText = data[i].name;


            unActeur.querySelector("img").setAttribute("src", this.imgPath + "w500" + data[i].profile_path);
            unActeur.querySelector("img").setAttribute("alt", data[i].name);


            document.querySelector(".liste-acteurs").appendChild(unActeur);
        }



    }

    //-------Caroussel

    // requeteFilmCaroussel(){
    //
    //
    //     var xhr = new XMLHttpRequest();
    //
    //
    //     xhr.addEventListener("readystatechange", this.retourFilmCaroussel.bind(this));
    //
    //     //xhr.open("GET", "https://api.themoviedb.org/3/movie/popular?page=1&language=en-US&api_key=%3C%3Capi_key%3E%3E");
    //     xhr.open("GET", this.baseURL + "movie/now_playing?page=1&language=" + this.lang + "&api_key=" + this.APIKey);
    //
    //     xhr.send();
    // }
    //
    //
    // retourFilmCaroussel(e){
    //
    //     var target = e.currentTarget;
    //
    //     var data;
    //
    //     if (target.readyState === target.DONE) {
    //
    //         data = JSON.parse(target.responseText).results;
    //
    //
    //
    //         this.afficheFilmCaroussel(data)
    //
    //     }
    //
    // }
    //
    //
    // afficheFilmCaroussel(data){
    //
    //
    //     console.log( data );
    //
    //     for(var i=0; i<this.totalFilm; i++){
    //
    //         var unArticle = document.querySelector(".template > .swiper-slide").cloneNode(true);
    //
    //
    //         unArticle.querySelector("img").setAttribute("src", this.imgPath + "w500" + data[i].poster_path);
    //         unArticle.querySelector("img").setAttribute("alt", data[i].title);
    //
    //         unArticle.querySelector("a").setAttribute("href", "fiche-film.html?id=" + data[i].id);
    //
    //         document.querySelector(".swiper-wrapper").appendChild(unArticle);
    //     }
    //
    // }



}